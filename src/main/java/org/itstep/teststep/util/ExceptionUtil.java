package org.itstep.teststep.util;

import lombok.experimental.UtilityClass;
import org.itstep.teststep.exception.NotFoundException;

@UtilityClass
public class ExceptionUtil {

    public static NotFoundException notFoundException(String message){
        return new NotFoundException(message);
    }

    public static IllegalArgumentException illegalArgumentException(String message) {
        return new IllegalArgumentException(message);
    }

}
