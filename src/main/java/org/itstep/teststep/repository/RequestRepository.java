package org.itstep.teststep.repository;

import org.itstep.teststep.entity.RequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<RequestEntity, Long> {

    List<RequestEntity> findAllByInitiatorId(Long customerId);

    List<RequestEntity> findAllByApproverId(Long customerId);
}
