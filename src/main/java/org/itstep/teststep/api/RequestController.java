package org.itstep.teststep.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.itstep.teststep.model.Request;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "/api/v1/Requests", description = "Manage Requests")
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/Requests")
public class RequestController {

    private org.itstep.teststep.service.RequestService RequestService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Return all existing Requests")
    public List<Request> getAllRequests() {
        return RequestService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return Request by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Request getRequestById(
            @ApiParam(value = "Id of a Request to lookup for", required = true)
            @PathVariable Long id) {
        return RequestService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create Request", notes = "Required Request instance")
    public Request createRequest(
            @ApiParam(value = "Request instance", required = true)
            @Valid @RequestBody Request Request) {
        return RequestService.save(Request);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete Request by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRequestById(
            @ApiParam(value = "Id of a Request to delete", required = true)
            @PathVariable Long id) {
        RequestService.delete(id);
    }

}
