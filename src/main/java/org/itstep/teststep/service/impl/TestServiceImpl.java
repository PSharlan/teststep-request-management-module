package org.itstep.teststep.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.itstep.teststep.entity.RequestEntity;
import org.itstep.teststep.mapper.RequestMapper;
import org.itstep.teststep.model.Request;
import org.itstep.teststep.repository.RequestRepository;
import org.itstep.teststep.service.RequestService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.itstep.teststep.util.ExceptionUtil.notFoundException;

//FIXME add logging
@Slf4j
@Service
@AllArgsConstructor
public class TestServiceImpl implements RequestService {

    private final RequestRepository repository;
    private final RequestMapper mapper;

    @Override
    public Request findById(Long id) {
        final RequestEntity request = repository.findById(id).orElseThrow(() ->
                notFoundException(String.format("Request with id %d not found.", id)));
        return mapper.mapToRequest(request);
    }

    @Override
    public List<Request> findByInitiatorId(Long customerId) {
        final List<RequestEntity> requests = repository.findAllByInitiatorId(customerId);

        //FIXME add method to mapper?
        return requests.stream().map(mapper::mapToRequest).collect(Collectors.toList());
    }

    @Override
    public List<Request> findByApproverId(Long customerId) {
        final List<RequestEntity> requests = repository.findAllByApproverId(customerId);

        //FIXME add method to mapper?
        return requests.stream().map(mapper::mapToRequest).collect(Collectors.toList());
    }

    @Override
    public List<Request> findAll() {
        final List<RequestEntity> requests = repository.findAll();

        //FIXME add method to mapper?
        return requests.stream().map(mapper::mapToRequest).collect(Collectors.toList());
    }

    @Override
    public Request save(Request request) {
        final RequestEntity requestToSave = mapper.mapToRequestEntity(request);
        final RequestEntity savedRequest = repository.save(requestToSave);
        return mapper.mapToRequest(savedRequest);
    }

    @Override
    public void delete(Long id) {
        //FIXME change to soft delete
        repository.deleteById(id);
    }
}
