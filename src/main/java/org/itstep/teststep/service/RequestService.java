package org.itstep.teststep.service;

import org.itstep.teststep.model.Request;

import java.util.List;

public interface RequestService {

    Request findById(Long id);

    List<Request> findByInitiatorId(Long customerId);

    List<Request> findByApproverId(Long customerId);

    List<Request> findAll();

    Request save(Request Request);

    void delete(Long id);

}
