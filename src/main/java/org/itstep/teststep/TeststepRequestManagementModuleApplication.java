package org.itstep.teststep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeststepRequestManagementModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeststepRequestManagementModuleApplication.class, args);
	}

}
