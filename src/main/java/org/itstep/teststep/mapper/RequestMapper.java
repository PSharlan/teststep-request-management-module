package org.itstep.teststep.mapper;

import org.itstep.teststep.entity.RequestEntity;
import org.itstep.teststep.model.Request;
import org.mapstruct.Mapper;

@Mapper
public interface RequestMapper {

    Request mapToRequest(RequestEntity request);

    RequestEntity mapToRequestEntity(Request request);
}
