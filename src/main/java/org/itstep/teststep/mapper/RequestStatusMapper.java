package org.itstep.teststep.mapper;

import org.itstep.teststep.entity.RequestStatusEntity;
import org.itstep.teststep.model.RequestStatus;
import org.mapstruct.Mapper;

@Mapper
public interface RequestStatusMapper {

    RequestStatus mapToStatus(RequestStatusEntity status);

    RequestStatusEntity mapToStatusEntity(RequestStatus status);
}
