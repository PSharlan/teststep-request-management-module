package org.itstep.teststep.mapper;

import org.itstep.teststep.entity.CustomerEntity;
import org.itstep.teststep.model.Customer;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {

    Customer mapToCustomer(CustomerEntity customer);

    CustomerEntity mapToCustomerEntity(Customer customer);
}
