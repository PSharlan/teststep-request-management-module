package org.itstep.teststep.mapper;

import org.itstep.teststep.entity.RequestEventEntity;
import org.itstep.teststep.model.RequestEvent;
import org.mapstruct.Mapper;

@Mapper
public interface RequestEventMapper {

    RequestEvent mapToEvent(RequestEventEntity event);

    RequestEventEntity mapToEventEntity(RequestEvent event);
}
