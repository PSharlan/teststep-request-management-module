package org.itstep.teststep.mapper;

import org.itstep.teststep.entity.RequestTypeEntity;
import org.itstep.teststep.model.RequestType;
import org.mapstruct.Mapper;

@Mapper
public interface RequestTypeMapper {

    RequestType mapToType(RequestTypeEntity type);

    RequestTypeEntity mapToTypeEntity(RequestType type);
}
