package org.itstep.teststep.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "request_entity")
public class RequestEntity {

    @Id
    private Long id;

    @Column(name = "type")
    private RequestTypeEntity type;
    @Column(name = "description")
    private String description;
    @Column(name = "approver")
    private CustomerEntity approver;
    @Column(name = "initiator")
    private CustomerEntity initiator;
    //FIXME create abstract class for all entities?
    @Column(name = "creation_time")
    private Date creationTime;
    @Column(name = "last_updating_time")
    private Date lastUpdatingTime;
    @Column(name = "closing_time")
    private Date closingTime;

    //FIXME
    @OneToMany
    private List<RequestEventEntity> history;
}
