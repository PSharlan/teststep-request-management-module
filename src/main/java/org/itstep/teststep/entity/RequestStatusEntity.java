package org.itstep.teststep.entity;

//FIXME @Entity?
public enum RequestStatusEntity {
    NEW(1L), IN_PROGRESS(2L), COMPLETED(3L), REJECTED(4L);

    private Long id;

    RequestStatusEntity(Long id) {
        this.id = id;
    }
}
