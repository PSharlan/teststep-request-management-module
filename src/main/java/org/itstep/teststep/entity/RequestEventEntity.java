package org.itstep.teststep.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class RequestEventEntity {
    //FIXME
    @Id
    private Long id;

    @Column(name = "request")
    private RequestEntity request;
    @Column(name = "description")
    private String description;
    @Column(name = "previous_status")
    private RequestStatusEntity previousStatus;
    @Column(name = "assigned_status")
    private RequestStatusEntity assignedStatus;
    @Column(name = "processed_approver")
    private CustomerEntity processedApprover;
    @Column(name = "assigned_status")
    private CustomerEntity assignedApprover;
    @Column(name = "creation_time")
    private Date creationTime;
}
