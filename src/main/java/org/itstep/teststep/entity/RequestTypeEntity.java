package org.itstep.teststep.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Data
@Entity
public class RequestTypeEntity {

    @Id
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    //FIXME
    @ManyToMany
    private List<CustomerEntity> availableApprovers;

}
