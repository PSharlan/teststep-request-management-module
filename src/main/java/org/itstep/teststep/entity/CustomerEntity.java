package org.itstep.teststep.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class CustomerEntity {

    @Id
    private Long id;

    @Column(name = "user_id")
    private Long userId;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
    //FIXME
    @Column(name = "role")
    private String role;

}
