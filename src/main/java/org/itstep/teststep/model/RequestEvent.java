package org.itstep.teststep.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

//FIXME validation
@Data
@Entity
public class RequestEvent {

    private Long id;

    private Request request;
    private String description;
    private RequestStatus previousStatus;
    private RequestStatus assignedStatus;
    private Customer processedApprover;
    private Customer assignedApprover;

    private Date creationTime;
}
