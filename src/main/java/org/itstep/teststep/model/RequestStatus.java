package org.itstep.teststep.model;

//FIXME validation
public enum RequestStatus {
    NEW(1L), IN_PROGRESS(2L), COMPLETED(3L), REJECTED(4L);

    private Long id;

    RequestStatus(Long id){
        this.id = id;
    }
}
