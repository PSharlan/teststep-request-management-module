package org.itstep.teststep.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
public class Customer {

    private Long id;

    @NotNull(message = "Customer w/o User ID")
    private Long userId;
    @NotNull(message = "Customer w/o name")
    private String name;
    @NotNull(message = "Customer w/o last name")
    private String lastName;
    //FIXME
    private String role;

}
