package org.itstep.teststep.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

//FIXME validation
@Data
@Entity
public class Request {
    private Long id;

    private RequestType type;
    private String description;
    private Customer approver;
    private Customer initiator;

    //FIXME
    private List<RequestEvent> history;

    //FIXME create abstract class for all entities?
    private Date creationTime;
    private Date lastUpdatingTime;
    private Date closingTime;

}
