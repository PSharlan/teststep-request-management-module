package org.itstep.teststep.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

//FIXME validation
@Data
public class RequestType {

    private Long id;

    private String name;
    private String description;
    private List<Customer> availableApprovers;

}
